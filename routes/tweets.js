var express=require('express');
var router=express.Router();
var sharedDB=require('../shared/db');


router.get('/get-data',function(req,res,next){
     sharedDB.fnGetDBCon(res,function(db){
          var collection=db.collection('Tweets');
          collection.find({}).toArray(function(e,r){
              if(e){
                  res.send(e);
              }else{
                res.send(r);
              }
          })
     })
})

module.exports=router;