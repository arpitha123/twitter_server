var mongodb=require('mongodb');
var mongoClient=mongodb.MongoClient;
var sharedDB={};
sharedDB.fnGetDBCon=function(res,cb){
   var url="mongodb://localhost:27017";
   mongoClient.connect(url,{ useUnifiedTopology: true },function(err,cluster){
       if(err){
           res.send('db con error',err);
       }else{
           var db=cluster.db('Twitter');
           cb(db);
       }
   })
}

module.exports=sharedDB;